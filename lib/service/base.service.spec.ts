import { BaseService } from './base.service';
import * as fs from 'fs-extra'
import { LookupListViewModel } from '../model/lookup-list-view.model';

const expectedCountryList = [{
    'id': '1',
    'code': 'USA',
    'description': 'United states',
    'isSelected': true
},
{
    'id': '2',
    'code': 'FR',
    'description': 'French',
    'isSelected': false
}
]
jest.mock('fs-extra', () => ({
    ...jest.requireActual('fs-extra')
}));

const fsExtra = require('fs-extra');


it('return file data - DEBUG is true', async () => {
    jest.spyOn(fsExtra, 'readJSON').mockImplementation(() => Promise.resolve(expectedCountryList))
    const baseService = new BaseService();
    process.env.DEBUG = 'true'
    const lookupListViewModel = new LookupListViewModel();
    lookupListViewModel.isError = false;
    lookupListViewModel.data = expectedCountryList;


    const data = await baseService.loadJson('path.json');
    expect(fsExtra.readJSON).toBeCalledWith(`${process.cwd()}/path.json`)
    expect(data).toEqual(lookupListViewModel);

});

it('return file data - DEBUG is false', async () => {
    jest.spyOn(fsExtra, 'readJSON').mockImplementation(() => Promise.resolve(expectedCountryList))
    const baseService = new BaseService();
    process.env.DEBUG = 'false'
    const lookupListViewModel = new LookupListViewModel();
    lookupListViewModel.isError = false;
    lookupListViewModel.data = expectedCountryList;

    const data = await baseService.loadJson('path.json');
    expect(fsExtra.readJSON).toBeCalledWith(`${process.cwd()}/function/path.json`)
    expect(data).toEqual(lookupListViewModel);

});


it('fail response ', async () => {
    const expectedValue = new Promise(() => {
        throw 'fail';
    });
    jest.spyOn(fsExtra, 'readJSON').mockImplementation(() => Promise.resolve(expectedValue))

    const baseService = new BaseService();
    process.env.DEBUG = 'true'
    const lookupListViewModel = new LookupListViewModel();
    lookupListViewModel.isError = true;
    lookupListViewModel.errorMessage = 'fail';

    const data = await baseService.loadJson('path');
    expect(data).toEqual(lookupListViewModel);

});


it('fail response - file not found', async () => {
    const expectedValue = new Promise(() => {
        throw { code: 'ENOENT' };
    });
    jest.spyOn(fsExtra, 'readJSON').mockImplementation(() => Promise.resolve(expectedValue))

    const baseService = new BaseService();
    process.env.DEBUG = 'true'
    const lookupListViewModel = new LookupListViewModel();
    lookupListViewModel.isError = true;
    lookupListViewModel.errorMessage = 'File not found!';

    const data = await baseService.loadJson('path');
    expect(data).toEqual(lookupListViewModel);

});



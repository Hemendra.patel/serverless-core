// import axios from 'axios';
import { Singleton } from 'typescript-ioc';
import { LookupListViewModel } from '../model/lookup-list-view.model';
import * as fs from 'fs-extra'
@Singleton
export class BaseService {
    async loadJson(path: string): Promise<LookupListViewModel> {
        const lookupListViewModel = new LookupListViewModel();
        const dataSourcePath =
            process.env.DEBUG === 'true' ? `${process.cwd()}/${path}` : `${process.cwd()}/function/${path}`;
        try {
            const data = await fs.readJSON(dataSourcePath);
            lookupListViewModel.isError = false;
            lookupListViewModel.data = data;
            return lookupListViewModel;
        } catch (error) {
            if (error.code === 'ENOENT') {
                lookupListViewModel.errorMessage = 'File not found!';
            } else {
                lookupListViewModel.errorMessage = error;
            }
            lookupListViewModel.isError = true;
            return lookupListViewModel;
        }
    }

}
import { BaseViewModel } from './base-view.model';
import { LookupModel } from './lookup.model';

export class LookupListViewModel extends BaseViewModel {
    data: LookupModel[] = [];
}

export class BaseViewModel {
    constructor() {
        this.isError = false;
    }
    isError: boolean;
    errorMessage?: string;
}

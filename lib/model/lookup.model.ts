export class LookupModel {
    constructor() {
        this.isSelected = false
    }
    id?: string;
    code?: string;
    description?: string;
    isSelected?: boolean;
}

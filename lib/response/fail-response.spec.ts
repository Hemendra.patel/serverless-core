import FunctionContext from 'openfaas-node-multiarch/lib/functionContext';
import { failresponse } from './fail-response';

it('fail response - DEBUG is true', async () => {
    process.env.DEBUG = 'true'
    const error = "file not found"
    const context = new FunctionContext()
    spyOn(context, 'fail')
    spyOn(context, 'status')
    failresponse(error, context)
    expect(context.fail).toBeCalled()
    expect(context.status).not.toBeCalled()
});

it('fail response - DEBUG is false', async () => {
    process.env.DEBUG = 'false'
    const error = "file not found"
    const context = new FunctionContext()
    spyOn(context, 'status').and.returnValue({ fail: () => { } })
    spyOn(context, 'fail')
    failresponse(error, context)
    expect(context.status).toBeCalledWith(500)
});
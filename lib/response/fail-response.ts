import { IFunctionContext } from 'openfaas-node-multiarch';


export function failresponse(error: any, context: IFunctionContext): void {
    return process.env.DEBUG === 'true' ? context.fail(error) : context.status(500).fail(error)
}

import { IFunctionContext } from 'openfaas-node-multiarch';
import { LookupListViewModel } from '../model/lookup-list-view.model';

export function successresponse(data: LookupListViewModel, context: IFunctionContext): void {
  if (process.env.DEBUG === 'true') {
    if (data.isError) {
      return context.succeed({ body: JSON.stringify(data.errorMessage), errorCode: 400 })
    } else {
      return context.succeed({ body: JSON.stringify(data.data) })
    }

  } else {
    if (data.isError) {
      return context.status(400).fail(data.errorMessage)
    } else {
      return context.status(200).succeed(data.data)
    }
  }
}


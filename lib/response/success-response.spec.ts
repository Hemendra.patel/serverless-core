import FunctionContext from 'openfaas-node-multiarch/lib/functionContext';
import { successresponse } from './success-response';
import { LookupListViewModel } from '../model/lookup-list-view.model';

it('success response - openfaas - DEBUG is false', async () => {
  const lookupListViewModel = new LookupListViewModel();
  lookupListViewModel.isError = false;
  process.env.DEBUG = 'false'
  const context = new FunctionContext()
  spyOn(context, 'status').and.returnValue({ succeed: () => { } })
  successresponse(lookupListViewModel, context)
  expect(context.status).toBeCalledWith(200)
});

it('fail response - openfaas- DEBUG is false', async () => {
  const lookupListViewModel = new LookupListViewModel();
  lookupListViewModel.isError = true;
  process.env.DEBUG = 'false'
  const context = new FunctionContext()
  spyOn(context, 'status').and.returnValue({ fail: () => { } })
  successresponse(lookupListViewModel, context)
  expect(context.status).toBeCalledWith(400)
});


it('success response - DEBUG is true', async () => {
  const lookupListViewModel = new LookupListViewModel();
  lookupListViewModel.isError = false;
  process.env.DEBUG = 'true'
  const context = new FunctionContext()
  spyOn(context, 'succeed')
  successresponse(lookupListViewModel, context)
  expect(context.succeed).toBeCalledWith({ "body": "[]" })
});

it('fail response - DEBUG is true', async () => {
  const lookupListViewModel = new LookupListViewModel();
  lookupListViewModel.isError = true;
  lookupListViewModel.errorMessage = 'Internal error';
  process.env.DEBUG = 'true'
  const context = new FunctionContext()
  spyOn(context, 'succeed')
  successresponse(lookupListViewModel, context)
  expect(context.succeed).toBeCalledWith({ "body": JSON.stringify(lookupListViewModel.errorMessage), "errorCode": 400 })
});